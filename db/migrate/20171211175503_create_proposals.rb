class CreateProposals < ActiveRecord::Migration[5.1]
  def change
    create_table :proposals do |t|
      t.string :name
      t.string :email
      t.text :content
      t.references :job, foreign_key: true

      t.timestamps
    end
  end
end
