class Job < ApplicationRecord
  belongs_to :employer
  has_many :proposals
  
  default_scope -> { order(created_at: :desc) }
  
  validates :title, :description, :employer, presence: true
end