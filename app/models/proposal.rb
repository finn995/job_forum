class Proposal < ApplicationRecord
  belongs_to :job, dependent: :destroy
  
  default_scope -> { order(created_at: :desc) }
  
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  
  validates :name, :content, :job, presence: true
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }
end