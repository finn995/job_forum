class JobsController < ApplicationController
  before_action :logged_in_employer, only: [:new, :create]
  
  def index
    @jobs = Job.all
  end

  def show
    @job = Job.find(params[:id])
    @current_job = @job
    @proposal = Proposal.new
  end

  def new
    @job = current_employer.jobs.build
  end
  
  def create
    @job = current_employer.jobs.build(job_params)
    if @job.save
      flash[:success] = 'Job created successfully'
      redirect_to @job
    else
      render 'new'
    end
  end
  
  private
  
    def job_params
      params.require(:job).permit(:title, :description)
    end
  
end