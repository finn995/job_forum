class SessionsController < ApplicationController
  
  def new
  end
  
  def create
    employer = Employer.find_by(email: params[:session][:email].downcase)
    if employer && employer.authenticate(params[:session][:password])
      log_in employer
      redirect_to employer
    else
      flash[:danger].now = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end

end