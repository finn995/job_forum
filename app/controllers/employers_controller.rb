class EmployersController < ApplicationController
  before_action :logged_in_employer, only: :show
  
  def show
    @employer = Employer.find(params[:id])
  end

  def new
    @employer = Employer.new
  end
  
  def create
    @employer = Employer.new(employer_params)
    if @employer.save
      log_in @employer
      flash[:success] = 'Create account successfully'
      redirect_to @employer
    else
      render 'new'
    end
  end
  
  private
    
    def employer_params
      params.require(:employer).permit(:name, :email, :password, :password_confirmation)
    end
  
end