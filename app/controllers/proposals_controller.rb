class ProposalsController < ApplicationController
	
	def create
		@job = Job.find_by(id: params[:current_job])
		@proposal = @job.proposals.build(proposal_params)
		if @proposal.save
			flash[:success] = 'proposal sent.'
			redirect_to jobs_path
		else
			render 'new'
		end
	end
	
	private
		
		def proposal_params
			params.require(:proposal).permit(:name, :email, :content)
		end
		
end