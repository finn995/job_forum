Rails.application.routes.draw do

  root 'jobs#index'
  get     '/login',    to: 'sessions#new'
  post    '/login',    to: 'sessions#create'
  delete  '/logout',   to: 'sessions#destroy'
  get     '/signup',   to: 'employers#new'
  resources :jobs
  resources :proposals, only: :create
  resources :employers, only: [:show, :create]
  
end