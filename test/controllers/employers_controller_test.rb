require 'test_helper'

class EmployersControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get employers_show_url
    assert_response :success
  end

  test "should get new" do
    get employers_new_url
    assert_response :success
  end

end
